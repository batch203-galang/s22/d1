// console.log("hello world");

// Array Methods

/*
	-JavaScript has built-in function and methods for arrays. This allows us to manipulate and acces array items.
	- Array can be either mutated or iterated.
		- Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.

*/

// Mutator Methods
// functions that mutate or change an array.
// These manipulates the original array performing various task such as adding and removing elements.

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
	- Adds an element in the end of an array AND returns the new array's length.
	-Syntax:
		arrayName.push(newElement);

*/

console.log("Current Array: ");
console.log(fruits);
// fruits,length = "Mango";
let fruitsLength = fruits.push("Mango"); //returns a new length of the array once we added an element.
console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

// Add multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: ");
console.log(fruits);

// pop()
/*
	- Removes the last element in an array AND returns the removed element.
	-Syntax:
		arrayName.pop();

*/
let removedFruit = fruits.pop();
console.log(removedFruit); //returns removed element
console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift()
/*
	- Add one or more elements at the beginning of an array.
	- Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA", "elementB");
		
*/

fruits.unshift("Lime","Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

// shift()
/*
	- Removes an element at the beginning of an array AND returns the removed element
	- Syntax
		arrayName.shift();

*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
	- simultaneously removes an element from a specified index number and adds elements.
	-Syntax:
		arrayName.splice(startingIndex, deletecount, elementsToBeAdded);

*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method: ");
console.log(fruits);

//sort()
/*
	- Rearranges the array elements in alphanumeric order
	- Syntax:
		- arrayName.sort();

*/

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

// reverse();
/*
	- Reverse the order of array elements
	- Syntax:
		arrayName.reverse();

*/

fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);


// NON-MUTATOR METHODS
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.

*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
/*
	- Returns the index of the first matching element found in an array.
	- If no match was found, the result will be -1
	- The search process will be done from the 1st element proceeding to the last element.
	-Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex/startingIndex);

*/
console.log(countries);
let firstIndex = countries.indexOf("PH");
//let firstIndex = countries.indexOf("PH", 2); //with a specific index start
console.log("Result of indexOf method: " +firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " +invalidCountry);

// lastIndexOf()
/*
	- Returns the index number of the last matching element found in an array.
	- The search from process will be done from the last element proceeding to the first element.
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex/endingIndex);

*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " +lastIndex);

// Getting the index of an array from a specific index number.
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
/*
	- Portions/slices element from an array AND returns a new array.
	- Syntax:
		arrayName.slice(startingIndex); //until the last element of the array.
		arrayName.slice(startingIndex, endingIndex);

*/

console.log("Original countries array:");
console.log(countries);

// Slicing off elements from specified index to the last element.
let sliceArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(sliceArrayA);

// Slicing off element from specified index to another index. But the specified last index is not included in the return.
let sliceArrayB = countries.slice(2, 5); //starting from 2 - 4 but not included si 5
console.log("Result from slice method:");
console.log(sliceArrayB);

// Slicing off elements starting from the last element of an array.
let sliceArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(sliceArrayC);

// toString()
/*
	- It returns an array as a string, seperated by commas.
	- Syntax:
		arrayName.toString();

*/

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
console.log(typeof stringArray); //to check if the array is converted to string

// concat()
/*
	
	- combines two arrays and returns the combined result.
	- Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(element);

*/

let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// Combining multiple arrays
console.log("Result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);


// Combine arrays with elements
let combinedTasks = tasksArrayA.concat("smell express","throw react");
console.log("Result from concat method:");
console.log(combinedTasks);

// join()
/*
	- it returns an array as a string seperated by specified seperator string.
	- Syntax:
		arrayName.join("seperatorString");

*/

let users = ["John", "Jane", "Doe", "Juan"];
console.log(users.join()); //default
console.log(users.join(' '));
console.log(users.join(" - "));

// Iteration Methods
/*
	- Iteration methods are loops designed to perform repetitive tasks on arrays.
	- It loops over all items/elements in an array.	
*/

// forEach()
/*
	- Similar a for loop that iterates on each array element.
	- for each item in the array, the anonymous function passed in the forEach() method will be run.
	- The anonymous function is able to received the current item being iterated or loop over.
	- Variable names for arrays are written in plural form of the data stored.
	- It's common practice to use the singular form of the array content for parameter names used in array loops.
	- forEach() does not return anything.

	- Syntax:
		arrayName.forEach(function(indivElement){
			//statement/code block
		})

*/

/*
	for(let i = 0; i < allTasks.length; i++){
		console.log(allTasks[i]);
	}
*/

// Using forEach with conditional statement.
let filteredTasks = []; //store in this variables all the task names with characters greater than 10.


				// anonymous function "parameter" represents each element of the array to be iterated.
allTasks.forEach(function(task){
	// It is a good practice to print the current element in the array console when working with array methods to have idea of what information is being worked on for each iteration.
	//console.log(task);

	// If the element/strings' length is greater than 10 characters
	if(task.length > 10){
		//console.log(task);

		// Add the element to the filtered task array.
		filteredTasks.push(task);
	}
})
console.log("Result of filtered tasks: ");
console.log(filteredTasks);

// map()
/*
	- Iterates on each element and returns new array with different values depending on the result of the function's operation.
	- This is useful for performing tasks where mutating/changing the elements required.

	- Syntax:
		let/const resultArray = arrayName.map(function(indivElement));

*/

let numbers = [1, 2, 3, 4, 5];

// return squared values of each element.
let numbersMap = numbers.map(function(number){
	return number*number;
})

console.log("Original Array: ");
console.log(numbers)//Original is not affected by map();

console.log("Result of the map method: ");
console.log(numbersMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	//console.log(number*number);
	return number*number;
})

console.log(numberForEach); //undefined
// forEach(), loops over all items in the array as does map, but forEach() does no return a new array.
// - pag printing nalang and walang manipulation forEach ang gagamitin, map pag may gusto ireturn.

// every() - its same with &&
/*
	- checks if all elements in an array meet the given condition.
	- This is useful for validation data stored in arrays especially when dealing with large amounts of data.
	- returns a true value if all elements meet the condition and false if otherwise.
	- Syntax:
		let/const = resultArray = arrayName.every(function(individual Element){
			return expression/condition;
		})

*/

numbers = [5, 7, 9, 0, 5];

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("Result of every method: ");
console.log(allValid);

// some() - its same with ||
/*
	- Check if atleast one element in the array meets the given condition.
	- Returns a true if at least one elements meets the condition and false otherwise.
	-Syntax:
		let/const resultArray = arrayName.some(function(individualElement){
			return expression/condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some method: ");
console.log(someValid);

// Combining return results from the every/some method may be used in other statements to perform consecutive result.

if(someValid){
	console.log("Some numbers in the array are greater than 2.");
}

// filter()
/*
	- Return a new array that contains elements which meets the given condition.
	- Return an empty array if no elements were found.
	- Useful for filtering array elements with ******
	- Syntax:
		let/const resultArray = arrayName.filter(function(individualElement){
			return expression/condition:
		})

*/

numbers = [5, 3, 3, 4, 5];

let filterValid = numbers.filter(function(number){
	return (number < 3);
})

console.log("Result of filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return number == 0;
})
console.log("Result of filter method: ");
console.log(nothingFound);

// shorten the syntax

filteredTasks = allTasks.filter(function(task){
	return (task.length > 10);
})

console.log("Result of filtered task using filter method: ");
console.log(filteredTasks);

// includes()
/*
	- Checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable
	- Syntax:
		let/const variableName = arrayName.includes(<argumentToFind>);

*/

let products = ["mouse", "keyboard", "laptop", "monitor"];
let productFound1 = products.includes("mouse");
console.log("Result of includes method: ");
console.log(productFound1);

let productNotFound = products.includes("headphones");
console.log("Result of includes method: ");
console.log(productNotFound);

// Method chaining
	// The result of the inner method is used in the second method until all "chained" methods have been resolved.

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('o');

	}) 
	console.log("Result of the chained method:");
	console.log(filteredProducts);

// reduce()
/*
	- Evaluates elements from left and right and returns/reduces the array into a single value.
	- Syntax:
		let/const resultVariable = arrayName.reduce(function(accumulator, currentValue){
			return expression/operation
		});
			- accumulator parameter stores the result for every loop.
			- currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop.

*/

let i = 0;
numbers = [1, 2, 3, 4, 5];


let reduceArray = numbers.reduce(function(acc, cur){
	console.warn("current iteration" + ++i);
	console.log("accumulator:" +acc);
	console.log("current value:" +cur);

	// The operation or expression to reduce the array into a single value.
	return acc + cur;
});

console.log("Result of reduce method: " +reduceArray);